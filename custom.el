(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(apheleia-global-mode t)
 '(column-number-mode nil)
 '(company-prescient-mode t)
 '(compilation-message-face (quote default))
 '(counsel-ag-base-command "/usr/local/bin/ag --nocolor --nogroup %s")
 '(cua-global-mark-cursor-color "#2aa198")
 '(cua-normal-cursor-color "#839496")
 '(cua-overwrite-cursor-color "#b58900")
 '(cua-read-only-cursor-color "#859900")
 '(custom-enabled-themes (quote (brutalist)))
 '(custom-safe-themes
   (quote
    ("9129c2759b8ba8e8396fe92535449de3e7ba61fd34569a488dd64e80f5041c9f" "5ed25f51c2ed06fc63ada02d3af8ed860d62707e96efc826f4a88fd511f45a1d" "97965ccdac20cae22c5658c282544892959dc541af3e9ef8857dbf22eb70e82b" "de1f10725856538a8c373b3a314d41b450b8eba21d653c4a4498d52bb801ecd2" "5e5345ea15d0c2234356bc5958a224776b83198f0c3df7155d1f7575405ce990" "6778eecfa231e177f2d4c0a72f4792ceffdfb96bf1bdfd73dcb210a4a619d13f" "fe16a59cc8d28255a61c701b032950a4785cc60708afebd352ed5960fcbc0e68" "96872a5b9e9a6b092df1e4bd034699c606c28f675869a8ff3ada1ca5f4d16ebf" "e04f0302582f9ee52cd2dac81149c3a9869d9640e05983bc4576c457da3b3492" "aaeb61894d5d3c586d27cee61a3f54493c383733f8d900e05c6fbc8ef6d6bc93" "4639288d273cbd3dc880992e6032f9c817f17c4a91f00f3872009a099f5b3f84" "c53b6a09c7d997c3185cb1598de1d0ff15e1679f5445f9a6cb8b2bf4fc4e565a" "436b185b423b78eb5d110dc23f4b95d78a1f002d156f226b7e6e5b1f6493dda0" "f0dc4ddca147f3c7b1c7397141b888562a48d9888f1595d69572db73be99a024" "d2e9c7e31e574bf38f4b0fb927aaff20c1e5f92f72001102758005e53d77b8c9" "100eeb65d336e3d8f419c0f09170f9fd30f688849c5e60a801a1e6addd8216cb" "6daa09c8c2c68de3ff1b83694115231faa7e650fdbb668bc76275f0f2ce2a437" "9be1d34d961a40d94ef94d0d08a364c3d27201f3c98c9d38e36f10588469ea57" "7559ac0083d1f08a46f65920303f970898a3d80f05905d01e81d49bb4c7f9e39" "e2fd81495089dc09d14a88f29dfdff7645f213e2c03650ac2dd275de52a513de" "7f89ec3c988c398b88f7304a75ed225eaac64efa8df3638c815acc563dfd3b55" "cd4d1a0656fee24dc062b997f54d6f9b7da8f6dc8053ac858f15820f9a04a679" "6ac7c0f959f0d7853915012e78ff70150bfbe2a69a1b703c3ac4184f9ae3ae02" "ed0b4fc082715fc1d6a547650752cd8ec76c400ef72eb159543db1770a27caa7" "7f3ef7724515515443f961ef87fee655750512473b1f5bf890e2dc7e065f240c" "2b6bd2ebad907ee42b3ffefa4831f348e3652ea8245570cdda67f0034f07db93" "5673c365c8679addfb44f3d91d6b880c3266766b605c99f2d9b00745202e75f6" "8d3c5e9ba9dcd05020ccebb3cc615e40e7623b267b69314bdb70fe473dd9c7a8" "10e231624707d46f7b2059cc9280c332f7c7a530ebc17dba7e506df34c5332c4" "d320493111089afba1563bc3962d8ea1117dd2b3abb189aeebdc8c51b5517ddb" default)))
 '(geiser-mode-smart-tab-p t t)
 '(global-hl-line-mode t)
 '(highlight-changes-colors (quote ("#d33682" "#6c71c4")))
 '(highlight-symbol-colors
   (--map
    (solarized-color-blend it "#002b36" 0.25)
    (quote
     ("#b58900" "#2aa198" "#dc322f" "#6c71c4" "#859900" "#cb4b16" "#268bd2"))))
 '(highlight-symbol-foreground-color "#93a1a1")
 '(highlight-tail-colors
   (quote
    (("#073642" . 0)
     ("#546E00" . 20)
     ("#00736F" . 30)
     ("#00629D" . 50)
     ("#7B6000" . 60)
     ("#8B2C02" . 70)
     ("#93115C" . 85)
     ("#073642" . 100))))
 '(hl-bg-colors
   (quote
    ("#7B6000" "#8B2C02" "#990A1B" "#93115C" "#3F4D91" "#00629D" "#00736F" "#546E00")))
 '(hl-fg-colors
   (quote
    ("#002b36" "#002b36" "#002b36" "#002b36" "#002b36" "#002b36" "#002b36" "#002b36")))
 '(lsp-ui-sideline-show-code-actions t)
 '(magit-diff-use-overlays nil)
 '(magit-todos-exclude-globs nil)
 '(magit-todos-fontify-keyword-headers t)
 '(magit-todos-ignore-case t)
 '(magit-todos-keywords (quote ("TODO" "FIXME" "KLUDGE" "HACK")))
 '(magit-todos-nice t)
 '(magit-todos-update 60)
 '(nrepl-message-colors
   (quote
    ("#dc322f" "#cb4b16" "#b58900" "#546E00" "#B4C342" "#00629D" "#2aa198" "#d33682" "#6c71c4")))
 '(olivetti-body-width 100)
 '(org-agenda-files nil)
 '(package-selected-packages
   (quote
    (cucumber-goto-step rubocop feature-mode ink-mode smartparens ripgrep evil-dvorak company-box lsp-go lsp-rust lsp-sh magit-circleci magithub company-tabnine company-terraform docker-compose-mode company-posframe ivy-posframe love-minor-mode ivy-prescient company-prescient prescient dap-go dap-mode lsp-javascript-typescript focus vagrant go-rename edit-indirect dockerfile-mode company-go go-guru go-errcheck ox-gfm htmlize zoom flymake-shellcheck terraform-mode anaconda-mode super-save ox-tiddly magit-popup worf helm-org-rifle company-lsp lsp-ui lsp-mode evil-magit ob-clojurescript forge which-key org-d20 scribble-mode helm-core org-wiki ereader brutalist-theme apples-mode tide json-mode json-navigator julia-repl geiser flycheck-racket fzf em-smart org-mode olivetti-mode smartparens-config olivetti org-variable-pitch quelpa-use-package quelpa pdfgrep magit-lfs magit-org-todos org-alert org-beautify-theme flycheck-julia flycheck-plantuml nim-mode ob-nim plantuml-mode god-mode org-context org-make-toc ox-tufte poet-theme evil-leader ob-applescript ob-rust ob-sml yasnippet-snippets flymake-racket slime slime-company flycheck-swiftlint swift-mode sotclojure aggressive-indent align-cljlet clj-refactor company-flow flow-minor-mode lispy lispyville clojure-snippets rjsx-mode flycheck-haskell flymake-hlint haskell-mode hasky-stack flycheck-clojure merlin key-chord hydra capture company-racer ess julia-mode company-lua flymake-lua lua-mode cmake-mode powershell pyenv-mode-auto gruvbox-theme ggtags cargo z3-mode yaml-mode writegood-mode vagrant-tramp utop tuareg sphinx-doc solarized-theme sml-mode smart-tab smart-mode-line rustfmt rainbow-delimiters racer pyenv-mode puppet-mode persp-projectile page-break-lines org-ref markdown-mode magit ivy-hydra hl-todo go-mode function-args flyspell-correct-ivy flycheck-rust flycheck-ocaml flycheck-irony flx evil-surround evil-snipe evil-org evil-nerd-commenter evil-matchit deft counsel-projectile counsel-osx-app counsel-dash company-irony company-auctex company-anaconda comment-dwim-2 boogie-friends better-defaults adaptive-wrap ace-link)))
 '(persp-show-modestring t)
 '(pos-tip-background-color "#073642")
 '(pos-tip-foreground-color "#93a1a1")
 '(safe-local-variable-values
   (quote
    ((projectile-project-run-cmd . "make serve")
     (projectile-project-test-cmd . "make verify")
     (eval add-to-list
           (quote exec-path)
           . "/Library/Java/JavaVirtualMachines/jdk1.8.0_192.jdk/Contents/Home/bin/")
     (eval add-to-list
           (quote exec-path)
           "/Library/Java/JavaVirtualMachines/jdk1.8.0_192.jdk/Contents/Home/bin/")
     (add-to-list
      (quote exec-path)
      "/Library/Java/JavaVirtualMachines/jdk1.8.0_192.jdk/Contents/Home/bin/"))))
 '(smartrep-mode-line-active-bg (solarized-color-blend "#859900" "#073642" 0.2))
 '(straight-use-package-by-default t)
 '(term-default-bg-color "#002b36")
 '(term-default-fg-color "#839496")
 '(vc-annotate-background nil)
 '(vc-annotate-background-mode nil)
 '(vc-annotate-color-map
   (quote
    ((20 . "#dc322f")
     (40 . "#ff7f00")
     (60 . "#ffbf00")
     (80 . "#b58900")
     (100 . "#ffff00")
     (120 . "#ffff00")
     (140 . "#ffff00")
     (160 . "#ffff00")
     (180 . "#859900")
     (200 . "#aaff55")
     (220 . "#7fff7f")
     (240 . "#55ffaa")
     (260 . "#2affd4")
     (280 . "#2aa198")
     (300 . "#00ffff")
     (320 . "#00ffff")
     (340 . "#00ffff")
     (360 . "#268bd2"))))
 '(vc-annotate-very-old-color nil)
 '(weechat-color-list
   (quote n
          (unspecified "#002b36" "#073642" "#990A1B" "#dc322f" "#546E00" "#859900" "#7B6000" "#b58900" "#00629D" "#268bd2" "#93115C" "#d33682" "#00736F" "#2aa198" "#839496" "#657b83")))
 '(xterm-color-names
   ["#073642" "#dc322f" "#859900" "#b58900" "#268bd2" "#d33682" "#2aa198" "#eee8d5"])
 '(xterm-color-names-bright
   ["#002b36" "#cb4b16" "#586e75" "#657b83" "#839496" "#6c71c4" "#93a1a1" "#fdf6e3"])
 '(z3-solver-cmd "/usr/local/bin/z3")
 '(zoom-size (quote (0.618 . 0.618))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(lsp-ui-sideline-code-action ((t (:foreground "blue2"))))
 '(lsp-ui-sideline-current-symbol ((t (:foreground "gray15" :box (:line-width -1 :color "gray15") :weight ultra-bold :height 0.99)))))
