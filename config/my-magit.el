(use-package magit
  :commands (magit-status)
  :init
  (global-set-key [f10] 'magit-status)
  :config ;; make magit work
  (setq
   magit-revert-buffers 'silent
   magit-completing-read-function 'ivy-completing-read))

(use-package forge
  :after magit)

(use-package magit-todos
  :after magit
  :config
  (setq magit-todos-require-colon nil
        magit-todos-update t
        magit-todos-depth 100
        magit-todos-nice nil
        magit-todos-keywords-list '("TODO" "XXX" "FIXME" "KLUDGE")
        magit-todos-scanner 'magit-todos--scan-with-rg))
(require 'magit-todos)
(magit-todos-mode)

(use-package forge
  :after magit)

(provide 'my-magit)
