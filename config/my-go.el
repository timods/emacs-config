;; Golang
(use-package go-mode
  :mode ("\\go$" . go-mode)
  :config
  (add-hook 'before-save-hook 'gofmt-before-save)
  (add-hook 'go-mode-hook
            (lambda ()
              (local-set-key (kbd "M-.") 'godef-jump)
              (local-set-key (kbd "M-*") 'pop-tag-mark)
              (setq indent-tabs-mode 1)
              (setq tab-width 2)))
  (add-hook 'go-mode-hook 'lsp-deferred)
  (require 'dap-go)
  (dap-go-setup)
  :bind (("C-." . godef-jump)))

(use-package company-go
  :after go-mode
  :config
  (eval-after-load 'company
    '(add-to-list 'company-backends 'company-go)))

(use-package go-rename
  :after go-mode)

(use-package go-guru
  :after go-mode)

(provide 'my-go)
