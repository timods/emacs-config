(use-package org-super-agenda
  :requires (org)
  )
(org-super-agenda-mode)
(straight-use-package '(org-ql :host github :repo "alphapapa/org-ql"))

(use-package origami
  :requires (org)
  :bind (:map org-super-agenda-header-map
              ("TAB" . origami-header-map)
              )
  :config

  (defvar ap/org-super-agenda-auto-show-groups
    '("Schedule" "Bills" "Priority A items" "Priority B items"))

  (defun ap/org-super-agenda-origami-fold-default ()
    "Fold certain groups by default in Org Super Agenda buffer."
    (forward-line 3)
    (cl-loop do (origami-forward-toggle-node (current-buffer) (point))
             while (origami-forward-fold-same-level (current-buffer) (point)))
    (--each ap/org-super-agenda-auto-show-groups
      (goto-char (point-min))
      (when (re-search-forward (rx-to-string `(seq bol " " ,it)) nil t)
        (origami-show-node (current-buffer) (point)))))

  :hook ((org-agenda-mode . origami-mode)
         (org-agenda-finalize . ap/org-super-agenda-origami-fold-default)))


(use-package org
  :mode (("\\.org$" . org-mode))
  :bind (("C-c a" . org-agenda)
         ("C-c c" . org-capture)
         ("C-c b" . org-iswitchb))
  :init
  (setq beorg-path "~/Library/Mobile Documents/iCloud~com~appsonthemove~beorg/Documents/org")
  (if (file-exists-p beorg-path)
      (setq org-default-notes-file (concat beorg-path "/inbox.org")
            org-agenda-files (list beorg-path))
    (setq org-default-notes-file "~/Documents/org/inbox.org"
          org-agenda-files (quote ( "~/Documents/org/" ) )))

  (add-to-list 'auto-mode-alist '("\\.\\(org\\|org_archive\\|txt\\)$" . org-mode))
  (setq org-support-shift-select t) ; Enable shift select for org mode.
  (setq org-adapt-indentation nil) ; Don't indent body text when demoting entries.
  (setq org-catch-invisible-edits 'smart) ; Don't allow invisible edits. This should be default!
  (setq org-hide-emphasis-markers t) ; Hide the markup for better reading.
  (setq org-startup-with-inline-images t) ; Display images by default.
  (setq org-image-actual-width '(400)) ; Make big images legible by default.
  (setq org-startup-with-latex-preview t) ; Display equations by default.
  (setq org-format-latex-options (plist-put org-format-latex-options :scale 1.3))

  :config
  (require 'org-mouse)
                                        ; I don't find the annotation useful.
  (font-lock-add-keywords               ; Make org list bullets pretty
   'org-mode
   '(("^ +\\([-*]\\) "
      (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))
  )

(use-package org-download ; Allow drag and drop support.
  :requires (org)
  :config
  (setq org-download-method 'attach) ; Use the awesome attachment system.
  (setq org-download-annotate-function (lambda (_) "")))

(provide 'my-org)
