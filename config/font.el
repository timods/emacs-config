(add-to-list 'default-frame-alist '(font . "Fira Code Retina 16"))

(set-face-attribute 'default nil :family "Fira Code Retina" :height 150)
;; (set-face-attribute 'fixed-pitch nil :family "Fira Code Retina")
;; (set-face-attribute 'variable-pitch nil :family "Charter")
(add-to-list 'default-frame-alist '(ns-transparent-titlebar . t))
(add-to-list 'default-frame-alist '(ns-appearance . light))
(add-hook 'text-mode-hook 'variable-pitch-mode)
(provide 'font)
;;; font.el ends here
