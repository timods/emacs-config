;; Fundamental functions
(add-to-list 'initial-frame-alist '(fullscreen . fullscreen))

(let ((home (getenv "HOME"))
      (gopath (getenv "GOPATH"))
      (path (getenv "PATH")))
  (let ((cargobin (concat home "/.cargo/bin"))
        (localbin (concat home "/.local/bin"))
        (gobin (concat home "/Documents/Projects/go" "/bin"))
        (brewbin (concat home "/homebrew/bin"))
        (brewsbin (concat home "/homebrew/sbin"))
        (usrlocalbin "/usr/local/bin"))
    (add-to-list 'exec-path cargobin)
    (add-to-list 'exec-path localbin)
    (add-to-list 'exec-path gobin)
    (add-to-list 'exec-path brewbin)
    (add-to-list 'exec-path brewsbin)
    (add-to-list 'exec-path usrlocalbin))
    (setenv "PATH" (mapconcat 'identity exec-path ":"))
  )
(setq ring-bell-function 'ignore)
(setq-default cursor-type 'bar)

(setq delete-by-moving-to-trash t)
(setq split-width-threshold 140) ; Be more preferable towards splitting horizontally.

;;; Backup and auto-save files
;; Save backup (~) files and auto save (#) files in a temp directory.
(let ((temp-emacs-dir (concat temporary-file-directory "Emacs/"))) ; Save to $TEMP/Emacs.
  (make-directory temp-emacs-dir t) ; Make sure $TEMP/Emacs exists.
  (setq auto-save-file-name-transforms `((".*" ,temp-emacs-dir t))) ; Auto save (#) files.
  (setq backup-directory-alist `((".*" . ,temp-emacs-dir)))) ; Backup (~) files.
(setq backup-by-copying t) ; Safer and reasonable for fast modern drives.
(setq version-control t) ; Let's keep a few backups of our files.
(setq delete-old-versions t) ; But let's not keep all backups.
(setq kept-new-versions 6) ; Just some new ones,
(setq kept-old-versions 2) ; and maybe some old ones..

(defun occur-dwim ()
  "Call `occur' with a sane default."
  (interactive)
  (push (if (region-active-p)
            (buffer-substring-no-properties
             (region-beginning)
             (region-end))
          (let ((sym (thing-at-point 'symbol)))
            (when (stringp sym)
              (regexp-quote sym))))
        regexp-history)
  (call-interactively 'occur))

(straight-use-package 'brutalist-theme)

(add-hook 'after-init-hook (lambda ()
                             (load-theme 'brutalist t t)))

(defun auto-chmod ()
  "If we're in a script buffer, then chmod +x that script."
  (and (save-excursion
         (save-restriction
           (widen)
           (goto-char (point-min))
           (save-match-data
             (looking-at "^#!"))))
       (shell-command (concat "chmod u+x " buffer-file-name))
       (message (concat "Saved as script: " buffer-file-name))))

(defun insert-date ()
  (interactive)
  (insert (shell-command-to-string "date +'%Y-%m-%d'")))

(defun endless/ispell-word-then-abbrev (p)
  "Call `ispell-word', then create an abbrev for it.
With prefix P, create local abbrev. Otherwise it will
be global."
  (interactive "P")
  (let ((bef (downcase (or (thing-at-point 'word)
                           "")))
        aft)
    (call-interactively 'ispell-word)
    (setq aft (downcase
               (or (thing-at-point 'word) "")))
    (unless (or (string= aft bef)
                (string= aft "")
                (string= bef ""))
      (message "\"%s\" now expands to \"%s\" %sally"
               bef aft (if p "loc" "glob"))
      (define-abbrev
        (if p local-abbrev-table global-abbrev-table)
        bef aft))))

(setq save-abbrevs t)
(setq-default abbrev-mode t)

(defun dcaps-to-scaps ()
  "Convert word in DOuble CApitals to Single Capitals."
  (interactive)
  (and (= ?w (char-syntax (char-before)))
       (save-excursion
         (and (if (called-interactively-p)
                  (skip-syntax-backward "w")
                (= -3 (skip-syntax-backward "w")))
              (let (case-fold-search)
                (looking-at "\\b[[:upper:]]\\{2\\}[[:lower:]]"))
              (capitalize-word 1)))))

(define-minor-mode dubcaps-mode
  "Toggle `dubcaps-mode'.  Converts words in DOuble CApitals to
Single Capitals as you type."
  :init-value nil
  :lighter (" DC")
  (if dubcaps-mode
      (add-hook 'post-self-insert-hook #'dcaps-to-scaps nil 'local)
    (remove-hook 'post-self-insert-hook #'dcaps-to-scaps 'local)))

(straight-use-package 'deadgrep)

(defun my-split-window-horizontally ()
  "Split window with another buffer."
  (interactive)
  (select-window (split-window-horizontally))
  (switch-to-buffer (other-buffer)))


;; Global keybindings
(define-key ctl-x-map "\C-i" 'endless/ispell-word-then-abbrev)
(global-set-key (kbd "C-x 3") 'my-split-window-horizontally)

;; (use-package undo-tree
;;   :init
;;   (setq undo-tree-history-directory-alist '( ("." . "~/.undo-tree" ) ))
;;
;;   :config
;;   (global-set-key (kbd "C-z") 'undo)
;;   ;; make ctrl-Z redo
;;   (defalias 'redo 'undo-tree-redo)
;;   (global-set-key (kbd "C-S-z") 'redo))
;;
;; (global-undo-tree-mode 1)
(use-package undo-fu
  :after evil
  :config
  (global-undo-tree-mode -1)
  (define-key evil-normal-state-map "u" 'undo-fu-only-undo)
  (define-key evil-normal-state-map "\C-r" 'undo-fu-only-redo))
(use-package undo-fu-session
  :config
  (setq undo-fu-session-incompatible-files '("/COMMIT_EDITMSG\\'" "/git-rebase-todo\\'")))

(global-undo-fu-session-mode)

;; Ensure that page nav doesn't leave the cursor at the bottom
(advice-add #'backward-page :after #'recenter)
(advice-add #'forward-page  :after #'recenter)
;; Environment settings
(set-language-environment "UTF-8")

(fset 'yes-or-no-p 'y-or-n-p)
;; GDB
(setq
 ;; use gdb-many-windows by default
 gdb-many-windows t
 ;; Non-nil means display source file containing the main routine at startup
 gdb-show-main t
 )

;; write a function to do the spacing
(defun simple-mode-line-render (left right)
  "Return a string of `window-width' length containing LEFT, and RIGHT
 aligned respectively."
  (let* ((available-width (- (window-width) (length left) 2)))
    (format (format " %%s %%%ds " available-width) left right)))

;; Mode-line
(setq-default mode-line-format
              '((:eval (simple-mode-line-render
                        ;; left
                        (format-mode-line (format "%s (%%l/%d) %%M"
                                                  (downcase (format-mode-line mode-name))
                                                  (line-number-at-pos (point-max))))
                        ;; right
                        (format "%s"
                                (concat (buffer-name)
                                        (cond
                                         ((not (buffer-file-name)) " ")
                                         ((buffer-modified-p) "*")
                                         (t " ")))
                                )
                        ))))


;; Hooks
(add-hook 'text-mode-hook #'dubcaps-mode)

;; Other Global Setup
(use-package better-defaults)

(use-package projectile
  :config
  (projectile-mode +1)
(setq
   projectile-completion-system 'ivy
   projectile-require-project-root 'prompt
   projectile-find-dir-includes-top-level t)
  )

(straight-use-package 'counsel-projectile)

(use-package flycheck
  :init (add-hook 'after-init-hook 'global-flycheck-mode)
  :config
  )

(use-package yasnippet
  :config
  (define-key yas-minor-mode-map (kbd "SPC") yas-maybe-expand)
  (yas-global-mode 1))

(use-package smartparens
  :config
  (smartparens-global-mode t)
  (sp-local-pair 'minibuffer-inactive-mode "'" nil :actions nil)
  (sp-local-pair 'minibuffer-inactive-mode "`" nil :actions nil)
  ;; Lisps
  (sp-local-pair 'emacs-lisp-mode "'" nil :actions nil)
  (sp-local-pair 'emacs-lisp-mode "`" nil :actions nil)
  (sp-local-pair 'racket-mode "'" nil :actions nil)
  (sp-local-pair 'racket-mode "`" nil :actions nil)
  (sp-local-pair 'lisp-interaction-mode "'" nil :actions nil)
  (sp-local-pair 'lisp-interaction-mode "`" nil :actions nil)
  (sp-local-pair 'scheme-mode "'" nil :actions nil)
  (sp-local-pair 'scheme-mode "`" nil :actions nil)
  (sp-local-pair 'inferior-scheme-mode "'" nil :actions nil)
  (sp-local-pair 'inferior-scheme-mode "`" nil :actions nil)
  ;;  Ocaml
  (sp-local-pair 'tuareg-mode "'" nil :actions nil)
  ;; *TeX
  (sp-local-pair 'LaTeX-mode "\"" nil :actions nil)
  (sp-local-pair 'LaTeX-mode "'" nil :actions nil)
  (sp-local-pair 'LaTeX-mode "`" nil :actions nil)
  (sp-local-pair 'latex-mode "\"" nil :actions nil)
  (sp-local-pair 'latex-mode "'" nil :actions nil)
  (sp-local-pair 'latex-mode "`" nil :actions nil)
  (sp-local-pair 'TeX-mode "\"" nil :actions nil)
  (sp-local-pair 'TeX-mode "'" nil :actions nil)
  (sp-local-pair 'TeX-mode "`" nil :actions nil)
  (sp-local-pair 'tex-mode "\"" nil :actions nil)
  (sp-local-pair 'tex-mode "'" nil :actions nil)
  (sp-local-pair 'tex-mode "`" nil :actions nil)
  ;; Rust
  (sp-local-pair 'rust-mode "<" ">")
  (sp-local-pair 'rust-mode "|" "|")
  (sp-with-modes '(c-mode c++-mode rust-mode)
    (sp-local-pair "{" nil :post-handlers '(("||\n[i]" "RET")))
    (sp-local-pair "/*" "*/" :post-handlers '((" | " "SPC")
                                              ("* ||\n[i]" "RET")))))
(use-package prescient
  :config
  (prescient-persist-mode 1))
(use-package ivy
  :demand
  :bind (("C-c C-r" . ivy-resume))
  :config
  (ivy-mode 1)
  (setq ivy-count-format "(%d/%d) ")
  (setq ivy-use-virtual-buffers t)
  (setq ivy-wrap t)
  (define-key ivy-minibuffer-map (kbd "<C-return>") 'ivy-immediate-done)
  (define-key ivy-mode-map (kbd "<C-return>") 'ivy-immediate-done))

(use-package swiper
  :after ivy
  :bind ("\C-s" . swiper-isearch))
(use-package counsel
  :after ivy
  :demand
  :bind (("M-x" . counsel-M-x)
         ("C-x C-f" . counsel-find-file)
         ("C-x l" . counsel-locate)
         ("C-c m" . counsel-imenu)
         ("C-c e" . counsel-find-file)
         ("C-c f" . counsel-rg)
         ("C-h b" . counsel-descbinds)))

(global-unset-key (kbd "C-x c"))

;; Company
(use-package company
  ;; :init (global-auto-complete-mode 0)
  :config
  (setq company-tooltip-flip-when-above t
        company-minimum-prefix-length 1
        company-idle-delay 0.0
        company-selection-wrap-around t
        company-show-numbers t
        company-require-match 'never
        company-dabbrev-downcase nil
        company-dabbrev-ignore-case t)
  (global-company-mode))

(company-tng-configure-default)
(setq company-frontends
      '(company-tng-frontend
        company-pseudo-tooltip-frontend
        company-echo-metadata-frontend))

(use-package comment-dwim-2
  :bind ("C-;" . comment-dwim-2))

(add-hook 'before-save-hook 'delete-trailing-whitespace)
(use-package rainbow-delimiters
  :commands (rainbow-delimiters-mode)
  :init (add-hook 'prog-mode-hook 'rainbow-delimiters-mode))

(use-package info
  :config
  (info-initialize))
(use-package hl-todo
  :config
  (global-hl-todo-mode)
  (setq hl-todo-keyword-faces '(("TODO" . hl-todo)
                                ("NOTE" . hl-todo)
                                ("XXX" . hl-todo)
                                ("FIXME" . hl-todo)
                                ("KLUDGE" . hl-todo))))


(use-package page-break-lines)
(global-page-break-lines-mode)

(use-package ace-link
  :config (ace-link-setup-default))

(use-package server)

;; Decrease keystroke echo timeout
(setq echo-keystrokes 0.5)
(setq line-number-display-limit-width 10000)

;; Backups
(setq vc-make-backup-files t)
(setq version-control t ;; Use version numbers for backups.
      kept-new-versions 10 ;; Number of newest versions to keep.
      kept-old-versions 0 ;; Number of oldest versions to keep.
      delete-old-versions t ;; Don't ask to delete excess backup versions.
      backup-by-copying t) ;; Copy all files, don't rename them.

;; Backup handling
(defvar my-auto-save-folder "~/.emacs.d/backup/per-save")

(defun force-backup-of-buffer ()
  ;; Make a special "per session" backup at the first save of each
  ;; emacs session.
  (when (not buffer-backed-up)
    ;; Override the default parameters for per-session backups.
    (let ((backup-directory-alist '(("" . "~/.emacs.d/backup/per-session")))
          (kept-new-versions 3))
      (backup-buffer)))
  ;; Make a "per save" backup on each save.  The first save results in
  ;; both a per-session and a per-save backup, to keep the numbering
  ;; of per-save backups consistent.
  (let ((buffer-backed-up nil))
    (backup-buffer)))
(add-hook 'before-save-hook 'force-backup-of-buffer)
(setq auto-save-file-name-transforms `((".*" ,my-auto-save-folder t)))
(setq tramp-auto-save-directory my-auto-save-folder)

;; wrapper for save-buffer ignoring arguments
(defun bjm/save-buffer-no-args ()
  "Save buffer ignoring arguments"
  (save-buffer))


(use-package which-key
  :demand)
(which-key-mode)

(require 'dash)



(global-hl-line-mode 1)
(global-auto-revert-mode 1)

(use-package zoom
  :demand
  :init
  (setq
   zoom-size '(0.618 . 0.618)
   zoom-mode t)
  :bind (("C-x +" . zoom)))

(use-package super-save
  :config
  (super-save-mode +1)
  (setq super-save-auto-save-when-idle t
        auto-save-default nil
        super-save-remote-files nil)

  ;; save on find-file
  (add-to-list 'super-save-hook-triggers 'find-file-hook))

(use-package olivetti
  :commands (olivetti-mode)
  :hook ((text-mode . olivetti-mode) (prog-mode . olivetti-mode))
  :config
  (setq olivetti-body-width 100))

(use-package company-prescient
  :after prescient)
(company-prescient-mode 1)
                                        ;(ivy-prescient-mode 1)

(use-package company-tabnine
  :after company
  :config
  (add-to-list 'company-backends #'company-tabnine))
(provide '0-setup)
