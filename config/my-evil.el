;; -*- lexical-binding: t -*-
;; Note: lexical-binding must be t in order for this to work correctly.
(defun make-conditional-key-translation (key-from key-to translate-keys-p)
  "Make a Key Translation such that if the translate-keys-p function returns true,
   key-from translates to key-to, else key-from translates to itself.  translate-keys-p
   takes key-from as an argument. "
  (define-key key-translation-map key-from
    (lambda (prompt)
      (if (funcall translate-keys-p key-from) key-to key-from))))
(defun my-translate-keys-p (key-from)
  "Returns whether conditional key translations should be active.  See make-conditional-key-translation function. "
  (and
   ;; Only allow a non identity translation if we're beginning a Key Sequence.
   (equal key-from (this-command-keys))
   (or (evil-motion-state-p) (evil-normal-state-p) (evil-visual-state-p))))

(use-package evil-leader
  :commands global-evil-leader-mode
  :after evil
  :config
  (evil-leader/set-leader "<SPC>")
  (evil-leader/set-key
    "a" 'org-agenda
    "c" 'org-capture
    "d" 'switch-to-buffer
    "e" 'counsel-find-file
    "f" 'deadgrep
    "g" 'hydra-go-guru/body
    "h" 'hydra-projectile/body
    "m" 'counsel-imenu
    "n" 'magit-status
    "o" 'hydra-org/body
    "q" 'hydra-describe/body
    "r" 'ivy-resume
    "t" 'hydra-windows/body
    "u" 'swiper-isearch
    "w" 'save-buffer
    "y" 'counsel-yank-pop
    "z" 'hydra-flycheck/body
    "<SPC>" 'counsel-M-x
    ";" 'avy-goto-word-1
    "'" 'cider-jack-in))

(use-package key-chord
  :commands key-chord-mode)
(use-package evil-surround
  :after evil
  :commands global-evil-surround-mode)
(use-package evil-matchit
  :after evil
  :commands global-evil-matchit-mode)

(defun my-save-if-bufferfilename ()
  (if (buffer-file-name) (progn (save-buffer))
    (message "no file is associated to this buffer: do nothing")))

(use-package evil
  :commands evil-mode
  :config
  (global-undo-tree-mode -1)
  (add-hook 'evil-insert-state-exit-hook 'my-save-if-bufferfilename)
  (dolist (modev (list
                  'magit-blame-mode
                  'magit-cherry-mode
                  'magit-diff-mode
                  'magit-log-mode
                  'magit-log-select-mode
                  'magit-mode
                  'magit-popup-help-mode
                  'magit-popup-mode
                  'magit-popup-sequence-mode
                  'magit-process-mode
                  'magit-reflog-mode
                  'magit-refs-mode
                  'magit-revision-mode
                  'magit-stash-mode
                  'elisp-mode
                  'lispy-mode
                  'clojure-mode
                  'clojurescript-mode
                  'magit-stashes-mode
                  'magit-status-mode))
    (evil-set-initial-state 'modev 'emacs))
  (setq evil-insert-state-modes
        (append evil-motion-state-modes
                '(deft-mode
                   magit-mode
                   eshell-mode
                   lispy-mode
                   clojure-mode
                   org-mode
                   geiser-mode
                   racket-mode
                   elisp-mode
                   cider-repl-mode
                   clojurescript-mode)))

;;; esc quits
  (dolist (mode (list
                 minibuffer-local-map
                 minibuffer-local-ns-map
                 minibuffer-local-completion-map
                 minibuffer-local-completion-map
                 minibuffer-local-must-match-map
                 minibuffer-local-isearch-map))
    (define-key mode [escape] 'minibuffer-keyboard-quit))
  (define-key evil-normal-state-map [escape] 'keyboard-quit)
  (define-key evil-visual-state-map [escape] 'keyboard-quit)
  (add-hook 'term-mode-hook 'evil-emacs-state)

  (defun my-move-key (keymap-from keymap-to key)
    "Moves key binding from one keymap to another, deleting from the old location. "
    (define-key keymap-to key (lookup-key keymap-from key))
    (define-key keymap-from key nil))
  (my-move-key evil-motion-state-map evil-normal-state-map (kbd "RET"))
  (my-move-key evil-motion-state-map evil-normal-state-map " ")

  ;; Emacs movement keys in insert mode
  (define-key evil-insert-state-map "\C-a" 'evil-beginning-of-line)
  (define-key evil-insert-state-map "\C-e" 'end-of-line)
  (define-key evil-insert-state-map "\C-n" 'evil-next-line)
  (define-key evil-insert-state-map "\C-p" 'evil-previous-line)
  (define-key evil-insert-state-map "\C-d" 'evil-delete)
  (define-key evil-insert-state-map "\C-d" 'evil-delete)

  ;; ergonomic keybindings

  ;; (define-key evil-normal-state-map "u" 'undo-tree-undo)
  ;; (define-key evil-normal-state-map "\C-r" 'undo-tree-redo)
  (define-key evil-normal-state-map "f" 'avy-goto-word-1)
  (define-key evil-normal-state-map "m" 'counsel-M-x)
  (define-key evil-normal-state-map "," 'counsel-M-x)
  (make-conditional-key-translation (kbd "t") (kbd "C-c") 'my-translate-keys-p)
  (make-conditional-key-translation (kbd ",") (kbd "C-x") 'my-translate-keys-p)



  ;; Better movement keys
  (define-key evil-normal-state-map "i" 'evil-insert)
  (define-key evil-normal-state-map "t" 'evil-previous-line)
  (define-key evil-normal-state-map "n" 'evil-backward-char)
  (define-key evil-normal-state-map "h" 'evil-next-line)
  (define-key evil-normal-state-map "s" 'evil-forward-char)

  (define-key evil-visual-state-map "i" 'evil-insert)
  (define-key evil-visual-state-map "t" 'evil-previous-line)
  (define-key evil-visual-state-map "n" 'evil-backward-char)
  (define-key evil-visual-state-map "h" 'evil-next-line)
  (define-key evil-visual-state-map "s" 'evil-forward-char)

  ;; Exit insert mode on jk
  (key-chord-mode 1)
  (key-chord-define evil-insert-state-map "jk" 'evil-normal-state)
  (global-evil-surround-mode 1)
  (global-evil-matchit-mode 1)
  )

(global-evil-leader-mode)
(evil-mode 1)

(use-package evil-magit
  :after magit
  :config
  (evil-define-key evil-magit-state magit-mode-map "i" 'evil-previous-line)
  (evil-define-key evil-magit-state magit-mode-map "j" 'evil-backward-char)
  (evil-define-key evil-magit-state magit-mode-map "k" 'evil-next-line)
  (evil-define-key evil-magit-state magit-mode-map "l" 'evil-forward-char)
  (evil-define-key evil-magit-state magit-mode-map "?" 'magit-dispatch)
  (evil-define-key evil-magit-state magit-mode-map "h" 'magit-log)
  )
(provide 'my-evil)
