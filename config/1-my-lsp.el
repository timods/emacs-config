;; ;; language server set up
(use-package posframe)
(require 'posframe)
(use-package ivy-posframe)
(use-package lsp-ui :commands lsp-ui-mode)
(use-package lsp-mode
  :after (evil)
  :commands lsp
  :hook ((rust-mode . lsp)
         (lsp-mode . lsp-enable-which-key-integration))
  :config
  (setq lsp-prefer-capf t
        lsp-prefer-flymake nil)
  (setq lsp-file-watch-ignored
        '(
          "[/\\\\]\\.direnv$"
          "[/\\\\]\\.gems$"
                                        ; SCM tools
          "[/\\\\]\\.git$"
          "[/\\\\]\\.hg$"
          "[/\\\\]\\.bzr$"
          "[/\\\\]_darcs$"
          "[/\\\\]\\.svn$"
          "[/\\\\]_FOSSIL_$"
                                        ; IDE tools
          "[/\\\\]\\.idea$"
          "[/\\\\]\\.vscode$"
          "[/\\\\]\\.ensime_cache$"
          "[/\\\\]\\.eunit$"
          "[/\\\\]node_modules$"
          "[/\\\\]\\.fslckout$"
          "[/\\\\]\\.tox$"
          "[/\\\\]\\.stack-work$"
          "[/\\\\]\\.bloop$"
          "[/\\\\]\\.metals$"
          "[/\\\\]target$"
          "[/\\\\]dist$"
                                        ; Autotools output
          "[/\\\\]\\.deps$"
          "[/\\\\]build-aux$"
          "[/\\\\]autom4te.cache$"
          "[/\\\\]\\.reference$"

                                        ; Assorted state dirs
          "[/\\\\]\\.terraform$"
          "[/\\\\]\\.vagrant$"
          "[/\\\\]vendor$"
          )))

(use-package dap-mode
  :config
  (add-hook 'dap-stopped-hook
            (lambda (arg) (call-interactively #'dap-hydra)))
  )



;; (dap-mode 1)
;; (dap-ui-mode 1)
;; ;; enables mouse hover support
;; (dap-tooltip-mode 1)
;; ;; use tooltips for mouse hover
;; ;; if it is not enabled `dap-mode' will use the minibuffer.
;; (tooltip-mode 1)
;; ;; displays floating panel with debug buttons
;; ;; requies emacs 26+
;; (dap-ui-controls-mode 1)

;; (defun my/window-visible (b-name)
;;   "Return whether B-NAME is visible."
;;   (-> (-compose 'buffer-name 'window-buffer)
;;       (-map (window-list))
;;       (-contains? b-name)))

;; (defun my/show-debug-windows (session)
;;   "Show debug windows."
;;   (let ((lsp--cur-workspace (dap--debug-session-workspace session)))
;;     (save-excursion
;;       ;; display locals
;;       (unless (my/window-visible dap-ui--locals-buffer)
;;         (dap-ui-locals))
;;       ;; display sessions
;;       (unless (my/window-visible dap-ui--sessions-buffer)
;;         (dap-ui-sessions)))))

;; (add-hook 'dap-stopped-hook 'my/show-debug-windows)

;; (defun my/hide-debug-windows (session)
;;   "Hide debug windows when all debug sessions are dead."
;;   (unless (-filter 'dap--session-running (dap--get-sessions))
;;     (and (get-buffer dap-ui--sessions-buffer)
;;          (kill-buffer dap-ui--sessions-buffer))
;;     (and (get-buffer dap-ui--locals-buffer)
;;          (kill-buffer dap-ui--locals-buffer))))

;; (add-hook 'dap-terminated-hook 'my/hide-debug-windows)

(provide '1-my-lsp)
