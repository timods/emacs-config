 (use-package rustic
   :config
   (setq lsp-rust-analyzer-server-command "~/.local/bin/rust-analyzer")
   (setq rustic-lsp-server 'rust-analyzer)
   (push 'rustic-clippy flycheck-checkers)
   (setq
    rustic-rustfmt-bin "/Users/timods/.cargo/bin/rustfmt"
    rustic-format-trigger 'on-save)
   :mode ("\\.rs$" . rustic-mode))

(provide 'my-rust)
