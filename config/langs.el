;;; langs.el --- configure language-specific settings. Could be broken out into lang-LANG.el if I cared
(blink-cursor-mode 0)


(use-package doom-snippets
  :straight (doom-snippets
             :type git
             :host github
             :repo "hlissner/doom-snippets")
  :after yasnippet)

;; Elisp
(add-to-list 'auto-mode-alist '("\\.el\\'" . emacs-lisp-mode))
;; Pico-8
(add-to-list 'auto-mode-alist '("\\.p8\\'" . lua-mode))

(defun s-trim-left (s)
  "Remove whitespace at the beginning of S."
  (if (string-match "\\`[ \t\n\r]+" s)
      (replace-match "" t t s)
    s))

(defun s-trim-right (s)
  "Remove whitespace at the end of S."
  (if (string-match "[ \t\n\r]+\\'" s)
      (replace-match "" t t s)
    s))

(defun s-trim (s)
  "Remove whitespace at the beginning and end of S."
  (s-trim-left (s-trim-right s)))

(use-package poly-markdown
  :init
  ;; https://polymode.github.io/usage/
  (add-to-list 'auto-mode-alist '("\\.txt$" . poly-markdown-mode))
  (add-to-list 'auto-mode-alist '("\\.markdown$" . poly-markdown-mode))
  (add-to-list 'auto-mode-alist '("\\.md$" . poly-markdown-mode)))
;; Markdown
(defvar markdown-code-languages
  '("haskell" "lisp" "javascript" "c" "rust" "go"))

(defun markdown-code-fence (beg end)
  "Make a code fence of the given region."
  (interactive "r")
  (save-excursion
    (goto-char beg)
    (goto-char (line-beginning-position))
    (insert "``` "
            (ido-completing-read "Language: " markdown-code-languages)
            "\n")
    (goto-char end)
    (goto-char (line-end-position))
    (newline)
    (insert "```")))
(defun shell-cmd (cmd)
  "Returns the stdout output of a shell command or nil if the command returned
   an error"
  (car (ignore-errors (apply 'process-lines (split-string cmd)))))

(defun reason-cmd-where (cmd)
  (let ((where (shell-cmd cmd)))
    (if (not (string-equal "unknown flag ----where" where))
      where)))

(use-package merlin
  :init
  (setq merlin-ac-setup t)
  )
(use-package reason-mode
  :init
  (let* ((refmt-bin (or (reason-cmd-where "refmt ----where")
                        (shell-cmd "which refmt")
                        (shell-cmd "which bsrefmt")))
         (merlin-bin (or (reason-cmd-where "ocamlmerlin ----where")
                         (shell-cmd "which ocamlmerlin")))
         (merlin-base-dir (when merlin-bin
                            (replace-regexp-in-string "bin/ocamlmerlin$" "" merlin-bin))))
    ;; Add merlin.el to the emacs load path and tell emacs where to find ocamlmerlin
    (when merlin-bin
      (add-to-list 'load-path (concat merlin-base-dir "share/emacs/site-lisp/"))
      (setq merlin-command merlin-bin))

    (when refmt-bin
      (setq refmt-command refmt-bin)))

  (add-hook 'reason-mode-hook (lambda ()
                                (add-hook 'before-save-hook 'refmt-before-save)
                                (merlin-mode)))
  )
;;----------------------------------------------------------------------------
;; Reason setup
;;----------------------------------------------------------------------------

(use-package markdown-mode
  :config
  (bind-keys :map markdown-mode-map
             ("C-c C-f" . markdown-code-fence)
             ("M-;" . markdown-blockquote-region)))
;; C, C++
(use-package ggtags
  :init
  (add-hook 'c-mode-common-hook
            (lambda ()
              (when (derived-mode-p 'c-mode 'c++-mode 'java-mode 'asm-mode)
                (require 'semantic)
                (global-semanticdb-minor-mode 1)
                (global-semantic-idle-scheduler-mode 1)
                (semantic-mode 1)
                (ggtags-mode 1)))))

(use-package irony
  :init
  (add-hook 'c++-mode-hook 'irony-mode)
  (add-hook 'c-mode-hook 'irony-mode)
  (add-hook 'objc-mode-hook 'irony-mode))
(use-package flycheck-irony
  :after irony
  :config
  (eval-after-load 'flycheck
    '(add-hook 'flycheck-mode-hook #'flycheck-irony-setup)))

(use-package company-irony
  :after irony
  :config
  (eval-after-load 'company
    '(add-to-list 'company-backends 'company-semantic))
  (eval-after-load 'company
    '(add-to-list 'company-backends 'company-irony)))
;; Python
(use-package python
  :mode ("\\.py$" . python-mode)
  :init
  (use-package sphinx-doc
    :commands sphinx-doc-mode)
  (use-package anaconda-mode)
  :config
  (setq python-shell-interpreter "ipython"
        python-shell-interpreter-args "--simple-prompt -i")
  (add-hook 'python-mode-hook (lambda ()
                                (require 'sphinx-doc)
                                (pyenv-mode)
                                (anaconda-mode)
                                (sphinx-doc-mode t)))
  )
(require 'dap-python)

;; HTML, html-templates
(use-package web-mode
  :init
  (add-to-list 'auto-mode-alist '("\\.html?" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.j2$" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.erb$" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.tmpl$" . web-mode))
  :config
  (setq web-mode-markup-indent-offset 2)
  (setq web-mode-css-indent-offset 2)
  (setq web-mode-code-indent-offset 2))

(use-package rjsx-mode
  :init
  (add-to-list 'auto-mode-alist '("\\.jsx?" . rjsx-mode))
  :config
  (add-hook 'rjsx-mode-hook
            (lambda ()
              (setq tab-width 2))))

(use-package yaml-mode
  :mode ("\\.yml$" . yaml-mode))

(use-package boogie-friends
  :mode ("\\z3$" . z3-smt2-mode))

(use-package aggressive-indent)

(use-package clojure-mode
  :init
  (setq cider-cljs-lein-repl
        "(do (require 'figwheel-sidecar.repl-api)
         (figwheel-sidecar.repl-api/start-figwheel!)
         (figwheel-sidecar.repl-api/cljs-repl))")
  :mode ("\\clj$" . clojure-mode)
  :config
  (require 'cider-common)
  (defun re-frame-jump-to-reg ()
    (interactive)
    (let* ((kw (cider-symbol-at-point 'look-back))
           (ns-qualifier (and
                          (string-match "^:+\\(.+\\)/.+$" kw)
                          (match-string 1 kw)))
           (kw-ns (if ns-qualifier
                      (cider-resolve-alias (cider-current-ns) ns-qualifier)
                    (cider-current-ns)))
           (kw-to-find (concat "::" (replace-regexp-in-string "^:+\\(.+/\\)?" "" kw))))

      (when (and ns-qualifier (string= kw-ns (cider-current-ns)))
        (error "Could not resolve alias \"%s\" in %s" ns-qualifier (cider-current-ns)))

      (progn (cider-find-ns "-" kw-ns)
             (search-forward-regexp (concat "reg-[a-zA-Z-]*[ \\\n]+" kw-to-find) nil 'noerror))))

  ;; (global-set-key (kbd "M->") 're-frame-jump-to-reg)
  (add-hook 'clojure-mode-hook (lambda () (lispy-mode 1)))
  (add-hook 'clojure-mode-hook #'rainbow-delimiters-mode)
  (add-hook 'clojure-mode-hook #'aggressive-indent-mode)
  (add-hook 'clojurescript-mode-hook (lambda () (lispy-mode 1)))
  (add-hook 'clojurescript-mode-hook #'rainbow-delimiters-mode)
  (add-hook 'clojurescript-mode-hook #'aggressive-indent-mode)
  (defun my-clojure-mode-hook ()
    (clj-refactor-mode 1)
    (yas-minor-mode 1)      ; for adding require/use/import statements
    ;; This choice of keybinding leaves cider-macroexpand-1 unbound
    (cljr-add-keybindings-with-prefix "C-c m"))
  (add-hook 'clojure-mode-hook #'my-clojure-mode-hook)
  )

(use-package lispy
  :config
  (add-hook 'emacs-lisp-mode-hook (lambda () (lispy-mode 1))))
(use-package lispyville
  :requires (lispy)
  :config
  (add-hook 'lispy-mode-hook #'lispyville-mode))

(use-package ess
  :init
  (require 'ess-site)
  :config
  (setq inferior-julia-program-name "/usr/local/bin/julia"))

(use-package sml-mode
  :config
  (add-to-list 'auto-mode-alist '("\\.sml$" . sml-mode))
  (add-to-list 'auto-mode-alist '("\\.sig$" . sml-mode)))

;; Scheme hacking
(use-package geiser
  :custom (geiser-mode-smart-tab-p t "No M-TAB garbage for me")
  :init (add-to-list 'auto-mode-alist '("\\.rkt$" . geiser-mode))
  :hook (scribble-mode)
  :bind (:map geiser-mode-map
              ("C-c '" . run-racket)))

(use-package scribble-mode
  :init
  (add-to-list 'auto-mode-alist '("\\.scrbl$" . scribble-mode)))

(use-package lua-mode
  :init
  (add-to-list 'auto-mode-alist '("\\.lua$" . lua-mode)))

(use-package love-minor-mode
   :requires (lua-mode)
  :hook (lua-mode)
  :after lua-mode)

(use-package terraform-mode
  :init (add-to-list 'auto-mode-alist '("\\.tf$" . terraform-mode)))

(use-package company-terraform
  :after company
  :config
  (add-to-list 'company-backends #'company-terraform))

(provide 'langs)
;;; langs.el ends here
