(use-package envrc
  :bind (:map envrc-mode-map
              ("C-c e" . envrc-command-map)))

(with-eval-after-load 'envrc
  (define-key envrc-mode-map (kbd "C-c e") 'envrc-command-map))

(envrc-global-mode)
( provide 'zz-fini )
